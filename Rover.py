import copy
import re

class Rover:
    def __init__(self, terrain, instructions, position, rover_name):
        self.terrain = terrain
        self.instructions = instructions
        self.starting_position = position
        self.position = copy.deepcopy(position)
        self.rover_name = rover_name

        if self.check_illegal_move() is True:
            raise Exception(self.rover_name + ' has landed in a wrong place')

    def move_robot(self):
        for instruction in list(self.instructions):
            try:
                self.follow_instruction(instruction)
            except Exception:
                print self.rover_name + " has made an illegal move"
                print "Returning rover to original landing site"
                self.position = copy.deepcopy(self.starting_position)
                break

    def follow_instruction(self, instruction):
        if instruction == 'M':
            self.position.move_forward()
        if instruction == 'L':
            self.position.turn_left()
        if instruction == 'R':
            self.position.turn_right()
        if self.check_illegal_move() is True:
            raise Exception('Rover made an illegal move')

    def check_illegal_move(self):
        return self.position.x_coordinate > self.terrain[0] or \
               self.position.x_coordinate < 0 or \
               self.position.y_coordinate > self.terrain[1] or \
               self.position.y_coordinate < 0

    def print_final_position(self):
        print self.rover_name + ':' + self.position.position_string()

