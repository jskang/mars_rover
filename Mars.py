import sys
import copy
import re
from Rover import Rover
from Position import Position

def get_landing_position(command):
    """Get the landing position of the rover from the input"""
    #  Check Landing position
    result = re.match('Rover[0-9]+ Landing:[ ]*[0-9]+[ ]+[0-9]*[ ]+[NSEW]{1}[ ]*\n',
                      command)
    # Get the name of the robot
    rover_name = get_rover_name(command)

    if result is None:
        raise Exception(rover_name + " Landing Position is malformatted")

    return command.split(':')[1].strip().split(' '), rover_name

def get_instructions(command):
    """Get the instructions of the rove from the input"""
    # Check Instructions
    result = re.match('Rover[0-9]+ Instructions:[ ]*[LRM]+[ ]*\n', command)
    # Get the name of the robot
    rover_name = get_rover_name(command)

    if result is None:
        raise Exception(rover_name + " Instruction is malformatted")

    return command.split(':')[1].strip(), rover_name

def get_rover_name(command):
    """Get the name of the rover from the commands"""
    return re.findall('Rover[0-9]+', command)[0]

def get_terrain(command):
    """ Get the terrain information form the commands"""
    # Check if the plateau command is correct
    output = re.match('plateau:[ ]*[0-9]+[ ]+[0-9]+[ ]*\n', command)
    if output is None:
        raise Exception("Plateau command is malformatted")

    # get the numbers for the terrain
    numbers = re.findall('[0-9]+', COMMANDS[0])
    return [int(number) for number in numbers]

if __name__ == "__main__":
    COMMANDS = []
    ROVERS = []
    # Read all the inputs
    for line in sys.stdin:
        COMMANDS.append(line)

    # Get the terrain information
    try:
        terrain = get_terrain(COMMANDS[0])
    except Exception as error:
        print error
        # if the commands are malformatted in anyway it will exit
        sys.exit(1)

    # Read the commands
    for i in xrange(1, len(COMMANDS), 2):
        try:
            rover_landing_position, rover_name_1 = get_landing_position(COMMANDS[i])
            rover_instuctions, rover_name_2 = get_instructions(COMMANDS[i + 1])
        except Exception as error:
            print error
            # If the commands are malformatted in any way it will exit
            sys.exit(1)

        # Rover name should match
        if rover_name_1 != rover_name_2:
            print "Rover Landing position and Rover Instructions are for different Rovers"
            # If the commands are malformatted in any way it will exit
            sys.exit(1)

        # Initialize rover's position
        rover_position = Position(int(rover_landing_position[0]),
                                  int(rover_landing_position[1]),
                                  rover_landing_position[2])

        # Create rover and ceck if it is correct
        try:
            rover = Rover(terrain, rover_instuctions, rover_position, rover_name_1)
            ROVERS.append(rover)
        except Exception as error:
            print error

    # Move the robots
    for rover in ROVERS:
        rover.move_robot()
        rover.print_final_position()
