class Position:
    def __init__(self, x_coordinate, y_coordinate, heading):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate

        self.possible_heading = ['N', 'E', 'S' ,'W']
        self.heading = self.possible_heading.index(heading)

    def turn_left(self):
        self.heading -= 1
        self.resolve_heading()

    def turn_right(self):
        self.heading += 1
        self.resolve_heading()

    def move_forward(self):
        if self.heading == 0:
            self.y_coordinate += 1
        elif self.heading == 1:
            self.x_coordinate += 1
        elif self.heading == 2:
            self.y_coordinate -= 1
        elif self.heading == 3:
            self.x_coordinate -= 1

    def resolve_heading(self):
        maximum_possible_heading = len(self.possible_heading) - 1
        if self.heading > maximum_possible_heading:
            self.heading = 0
        if self.heading < 0:
            self.heading = maximum_possible_heading

    def print_position(self):
        print self.x_coordinate, self.y_coordinate, self.possible_heading[self.heading]

    def position_string(self):
        return str(self.x_coordinate) + " " + str(self.y_coordinate) + " " + \
               self.possible_heading[self.heading]



